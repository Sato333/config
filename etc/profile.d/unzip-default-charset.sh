locale=${LC_ALL:-$LC_CTYPE}
locale=${locale:-$LANG}
locale=${locale%%.*}

case "$locale" in
    ja_JP)
        charset=CP932
    ;;
    ko_KR)
        charset=CP949
    ;;
    th_TH)
        charset=CP874
    ;;
    vi_VN)
        charset=CP1258
    ;;
    zh_CN)
        charset=CP936
    ;;
    zh_TW)
        charset=CP950
    ;;
    *)
        charset=
    ;;
esac

if [ -n "$charset" ]; then
    export UNZIP="-O $charset"
    export ZIPINFO="-O $charset"
fi
