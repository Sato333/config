TARGETS = console-setup mountkernfs.sh alsa-utils ufw lm-sensors hostname.sh dns-clean x11-common apparmor plymouth-log pppd-dns udev keyboard-setup resolvconf mountdevsubfs.sh procps brltty networking hwclock.sh urandom checkroot.sh mountall-bootclean.sh mountall.sh bootmisc.sh checkroot-bootclean.sh mountnfs.sh checkfs.sh kmod mountnfs-bootclean.sh
INTERACTIVE = console-setup udev keyboard-setup checkroot.sh checkfs.sh
udev: mountkernfs.sh
keyboard-setup: mountkernfs.sh udev
resolvconf: dns-clean
mountdevsubfs.sh: mountkernfs.sh udev
procps: mountkernfs.sh udev
brltty: mountkernfs.sh udev
networking: mountkernfs.sh urandom resolvconf dns-clean procps
hwclock.sh: mountdevsubfs.sh
urandom: hwclock.sh
checkroot.sh: hwclock.sh keyboard-setup mountdevsubfs.sh hostname.sh
mountall-bootclean.sh: mountall.sh
mountall.sh: checkfs.sh checkroot-bootclean.sh
bootmisc.sh: mountall-bootclean.sh checkroot-bootclean.sh mountnfs-bootclean.sh udev
checkroot-bootclean.sh: checkroot.sh
mountnfs.sh: networking
checkfs.sh: checkroot.sh
kmod: checkroot.sh
mountnfs-bootclean.sh: mountnfs.sh
